Modernizr.on('webp', function (result) {
  var img = document.getElementsByTagName('img');
  for(var i = 0; i < img.length; i++)
  {
	if(img[i].className !== "interest-img")
	{
		if (result && img[i].getAttribute('data-webp') != null) {
		  img[i].src = img[i].getAttribute('data-webp');
		}
		else {
			img[i].src = img[i].getAttribute('data-common');
		}
	}
  }
});
